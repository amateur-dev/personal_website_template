import React from 'react';
// import './App.css';
import About from './components/About'
import Header from './components/Header'
import Footer from './components/Footer'
function App() {
  return (
    <div id="colorlib-page">

      <Header />
      <About />


      <Footer />
    </div>
  );
}

export default App;
