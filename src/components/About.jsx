import React, { PureComponent } from 'react';


class About extends PureComponent {
	constructor(props) {
		super(props)
		// this.state = []
	}

	render() {
		return (

			<div id="colorlib-about">
				<div className="container">
					<div className="row text-center">
						<h2 className="bold">About</h2>
					</div>
					<div className="row row-padded-bottom">
						<div className="col-md-5 animate-box">
							<img className="img-responsive about-img" src="images/about.jpg" alt="html5 bootstrap template by colorlib.com" />
						</div>
						<div className="col-md-6 col-md-push-1 animate-box">
							<div className="about-desc">
								<h2><span>Desi Code</span><span>Club</span></h2>
								<div className="desc">
									<div className="rotate">
										<h2 className="heading">About</h2>
									</div>
									<p>A simple coding club formed by Dipesh and Tosh to help our fellow coders in creating world class web applications and build and international scale career</p>
									<br />
									<p>Insert Text</p>
									<p className="colorlib-social-icons">
										{/* <a href="#"><i className="icon-facebook4"></i></a> */}
										<a href="https://twitter.com/#"><i className="icon-twitter"></i></a>
										<a href="https://gitlab.com/#"><i className="icon-gitlab"></i></a>
										<a href="https://www.linkedin.com/#"><i className="icon-linkedin"></i></a>
										<a href="#"><i className="icon-blog"></i></a>
									</p>
									{/* <p><a href="work.html" className="btn btn-primary btn-outline">View My Works</a></p> */}
								</div>
							</div>
						</div>
					</div>
					{/* <div className="row">
						<div className="col-md-4 animate-box">
							<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						</div>
						<div className="col-md-4 animate-box">
							<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						</div>
						<div className="col-md-4 animate-box">
							<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
						</div>
					</div> */}
				</div>
			</div>

		)
	}

}

export default About;


